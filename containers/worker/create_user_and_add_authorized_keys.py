#! /usr/bin/python3.5
""" Creates user, assumes root privileges"""

import subprocess
import os
import argparse
import re
import shlex

USER_CREATED_MESSAGE = "User created"

def create_user_check_security_add_keys_and_handle_exception(username, uid, group, auth_key):
    """ Creates the linux user with the given parameters,
    checks security vulnerabilities, adds ssh keys and handles exceptions.

    Args:
        username: The user's username
	uid: The user's uid
	group: Comma separated string with the groups in which the user belongs
	auth_key: The user's public key

    Returns:
	USER_CREATED_MESSAGE constant if the user was created succesfully.
        Exception message, in case the user was not created.
    """
    try:
        return check_security_create_user_and_add_keys(username, uid, group, auth_key)
    except Exception as exception:
        return handle_user_creation_exception(exception)

def check_security_create_user_and_add_keys(username, uid, group, auth_key):
    """ Checks security, sanitizes the inputs, creates the linux user and adds the authorized keys

    Args:
        username: The user's username
	uid: The user's uid
	group: Comma separated string with the groups in which the user belongs
	auth_key: The user's public key

    Returns:
	USER_CREATED_MESSAGE constant if the user was created succesfully.

    Raises:
        Exception:
    """
    username, uid, group, auth_key = check_security_and_sanitize_request(username,
                                                                         uid,
                                                                         group,
                                                                         auth_key)
    create_user_with_home_folder(username, uid, group)
    add_authorized_keys(username, auth_key)
    return USER_CREATED_MESSAGE

def check_security_and_sanitize_request(username, uid, group, auth_key):
    """ Checks the arguments provided by the user to ensure they
    don't contain a threat to the system.

    Args:
        username: The user's username
	uid: The user's uid
	group: Comma separated string with the groups in which the user belongs
	auth_key: The user's public key

    Returns:
	A tuple with sanitized username, uid, group and auth_key

    Raises:
        Exception: In case the group contains sudo or admin keyword.
    """

    def check_group(group):
        """ Checks the user does not request sudo or admin access"""
        sudo_matcher = re.compile(".*sudo.*")
        admin_matcher = re.compile(".*admin.*")
        if sudo_matcher.match(group) or admin_matcher.match(group):
            raise Exception("Not possible to create sudo user")

    def check_uid(uid):
        """ Checks the uid is not 0 (root)"""
        if int(uid) == 0:
            raise Exception("Not possible to create root user")

    def sanitize_inputs(username, uid, group, auth_key):
        """ Sanitizes inputs by quoting the parameters to avoid injection """
        return (shlex.quote(x) for x in [username, uid, group, auth_key])

    check_group(group)
    check_uid(uid)
    return sanitize_inputs(username, uid, group, auth_key)

def create_user_with_home_folder(username, uid, group):
    """ Creates the user with the given parameter and creates its home folder

    Args:
        username: The user's username
	uid: The user's uid
	group: Comma separated string with the groups in which the user belongs

    Returns:
        None

    Raises:
        Exception: In case useradd command exits with non-zero exit status
    """

    create_user_system_command = "useradd --create-home --groups {2} --uid {1} {0}".format(username, uid, group)
    try:
        subprocess.run(create_user_system_command, shell=True, check=True)
    except Exception as exception:
        useradd_dictionary = dict([("0", "Success"),
                                   ("1", "Can't update password file"),
                                   ("2", "Invalid command syntax "),
                                   ("3", "Invalid argument to option"),
                                   ("4", "UID already in use (and no -o) "),
                                   ("6", "Specified group doesn't exist "),
                                   ("9", "Username already in use"),
                                   ("10", "Can't update group file"),
                                   ("12", "Can't create home directory "),
                                   ("14", "Can't update SELinux user mapping")])
        raise Exception(useradd_dictionary[str(exception.args[0])])

def add_authorized_keys(username, auth_key):
    """ Adds the user's authorized keys by creating the ssh folder, fixing its permissions,
    and adding the authorized_keys file

    Args:
        username: The user's username
	auth_key: The user's ssh public key

    Returns:
        None

    Raises:
        Exception: In case the user's ssh folder permissions cannot be modified
    """

    def create_ssh_folder(username):
        """ Creates the user's ssh folder """
        os.makedirs("/home/{0}/.ssh".format(username))

    def fix_ssh_folder_permissions(username):
        """ Fixes the user's ssh folder permissions """
        fix_ssh_folder_permissions_system_command = "chmod 700 /home/{0}/.ssh".format(username)
        subprocess.run(fix_ssh_folder_permissions_system_command, shell=True, check=True)

    def add_authorized_key(username, auth_key):
        """ Adds the user's authorized keys to the corresponding file """
        with open("/home/{0}/.ssh/authorized_keys".format(username), "a") as authorized_keys:
            authorized_keys.write(auth_key)

    create_ssh_folder(username)
    fix_ssh_folder_permissions(username)
    add_authorized_key(username, auth_key)

def handle_user_creation_exception(exception):
    """ Handles the exceptions by returning it's message"""
    return exception.args[0]

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--username", required=True)
    parser.add_argument("--uid", required=True)
    parser.add_argument("--group", required=True)
    parser.add_argument("--key", required=True)

    args = parser.parse_args()

    result = create_user_check_security_add_keys_and_handle_exception(username=args.username,
                                                                      uid=args.uid,
                                                                      group=args.group,
                                                                      auth_key=args.key)
    print(result)
