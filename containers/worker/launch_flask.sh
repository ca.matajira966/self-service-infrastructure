#! /bin/bash
source /etc/environment
export FLASK_APP=app.py
export FLASK_ENV=development
sleep 10
flask db migrate -m"task table"
flask db migrate -m"result table"
flask db upgrade
flask run --host=0.0.0.0 --port=8080
