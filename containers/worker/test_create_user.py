
import unittest
import create_user_and_add_authorized_keys as create
import subprocess


class TestNormalUsage(unittest.TestCase):
    def setUp(self):
        self.username = "new_user_test"
        self.uid = "12345"
        self.group = "new_group_test"
        self.auth_key = "auth_key"
        subprocess.run("addgroup new_group_test", capture_output=True, shell=True)

    def tearDown(self):
        subprocess.run("rm -rf /home/new_user_test/", capture_output=True, shell=True)
        subprocess.run("deluser new_user_test", capture_output=True, shell=True)
        subprocess.run("delgroup new_group_test", capture_output=True, shell=True)

    def test_create_user_check_security_add_keys_and_handle_exception_assert____user(self):
        create.create_user_check_security_add_keys_and_handle_exception(username=self.username, uid=self.uid, group=self.group, auth_key=self.auth_key)
        self.assertTrue(subprocess.run("grep 'new_user_test' /etc/passwd", capture_output=True, shell=True).returncode == 0)

    def test_create_user_check_security_add_keys_and_handle_exception_assert____user_and_return_message(self):
        result = create.create_user_check_security_add_keys_and_handle_exception(username=self.username, uid=self.uid, group=self.group, auth_key=self.auth_key)
        self.assertEqual(result, create.USER_CREATED_MESSAGE)


    def test_create_user_check_security_add_keys_and_handle_exception_assert____group(self):
        create.create_user_check_security_add_keys_and_handle_exception(username=self.username, uid=self.uid, group=self.group, auth_key=self.auth_key)
        self.assertTrue(subprocess.run("egrep 'new_group_test.*new_user_test' /etc/group", capture_output=True, shell=True).returncode == 0)

    def test_create_user_check_security_add_keys_and_handle_exception_assert____uid(self):
        create.create_user_check_security_add_keys_and_handle_exception(username=self.username, uid=self.uid, group=self.group, auth_key=self.auth_key)
        self.assertTrue(subprocess.run("egrep 'new_user_test:x:12345.*' /etc/passwd", capture_output=True, shell=True).returncode == 0)

    def test_create_user_check_security_add_keys_and_handle_exception_assert____home_created(self):
        create.create_user_check_security_add_keys_and_handle_exception(username=self.username, uid=self.uid, group=self.group, auth_key=self.auth_key)
        self.assertTrue(subprocess.run("bash -c '[[ -d /home/new_user_test/ ]]'", capture_output=True, shell=True).returncode == 0)

    def test_create_user_check_security_add_keys_and_handle_exception_assert____dot_ssh_folder(self):
        create.create_user_check_security_add_keys_and_handle_exception(username=self.username, uid=self.uid, group=self.group, auth_key=self.auth_key)
        self.assertTrue(subprocess.run("bash -c '[[ -d /home/new_user_test/.ssh ]]'", capture_output=True, shell=True).returncode == 0)

    def test_create_user_check_security_add_keys_and_handle_exception_assert____dot_ssh_folder_permissions(self):
        create.create_user_check_security_add_keys_and_handle_exception(username=self.username, uid=self.uid, group=self.group, auth_key=self.auth_key)
        ssh_folder_permissions = subprocess.run("bash -c 'stat -c \"%a\" /home/new_user_test/.ssh'", capture_output=True, shell=True).stdout
        self.assertEqual(int(ssh_folder_permissions), 700)

    def test_create_user_check_security_add_keys_and_handle_exception_assert____authorized_keys(self):
        create.create_user_check_security_add_keys_and_handle_exception(username=self.username, uid=self.uid, group=self.group, auth_key=self.auth_key)
        with open("/home/new_user_test/.ssh/authorized_keys", "r") as authorized_keys_content:
            public_key_in_file = authorized_keys_content.readline()
            self.assertEqual(self.auth_key, public_key_in_file)

    def test_create_user_check_security_add_keys_and_handle_exception_assert____permissions_authorized_keys(self):
        create.create_user_check_security_add_keys_and_handle_exception(username=self.username, uid=self.uid, group=self.group, auth_key=self.auth_key)
        authorized_keys_permissions = subprocess.run("bash -c 'stat -c \"%a\" /home/new_user_test/.ssh/authorized_keys'", capture_output=True, shell=True).stdout
        self.assertEqual(int(authorized_keys_permissions), 644)

    def test_create_user_check_security_add_keys_and_handle_exception_assert____good_exception_message(self):
        create.create_user_check_security_add_keys_and_handle_exception(username=self.username, uid=self.uid, group=self.group, auth_key=self.auth_key)
        result = create.create_user_check_security_add_keys_and_handle_exception(username=self.username, uid=self.uid, group=self.group, auth_key=self.auth_key)
        self.assertEqual(result, "Username already in use")

class TestSecurity(unittest.TestCase):
    def setUp(self):
        self.username = "new_user_test"
        self.uid = "12345"
        self.group = "new_group_test"
        self.auth_key = "auth_key"
        subprocess.run("addgroup new_group_test", capture_output=True, shell=True)

    def tearDown(self):
        subprocess.run("rm -rf /home/new_user_test/", capture_output=True, shell=True)
        subprocess.run("deluser new_user_test", capture_output=True, shell=True)
        subprocess.run("delgroup new_group_test", capture_output=True, shell=True)
        subprocess.run("rm /tmp/i_was_hacked", capture_output=True, shell=True)

    def test_not_possible_to_create_user_in_sudo_group(self):
        result = create.create_user_check_security_add_keys_and_handle_exception(username=self.username, uid=self.uid, group="sudo", auth_key=self.auth_key)
        self.assertEqual(result, "Not possible to create sudo user")

    def test_check_security_of_request___no_sudo(self):
        with self.assertRaises(Exception):
            create.check_security_of_request(username=self.username, uid=self.uid, group="sudo", auth_key=self.auth_key)

    def test_check_security_of_request___no_sudo_in_one_of_the_groups(self):
        with self.assertRaises(Exception):
            create.check_security_of_request(username=self.username, uid=self.uid, group="users, sudo", auth_key=self.auth_key)

    def test_check_security_of_request___no_sudo_in_one_of_the_groups_2(self):
        with self.assertRaises(Exception):
            create.check_security_of_request(username=self.username, uid=self.uid, group="users,sudo,www-data", auth_key=self.auth_key)

    def test_check_security_of_request___no_admin_in_one_of_the_groups(self):
        with self.assertRaises(Exception):
            create.check_security_of_request(username=self.username, uid=self.uid, group="users,admin,www-data", auth_key=self.auth_key)

    def test_check_security_of_request___no_uid_0(self):
        with self.assertRaises(Exception):
            create.check_security_of_request(username=self.username, uid="0", group=self.group, auth_key=self.auth_key)

    def test_check_security_of_request___no_uid_0_2(self):
        with self.assertRaises(Exception):
            create.check_security_of_request(username=self.username, uid=0, group=self.group, auth_key=self.auth_key)

    def test_check_security_of_request___shell_sanitation_1(self):
        create.create_user_check_security_add_keys_and_handle_exception(username="new_user_test; touch /tmp/i_was_hacked", uid=self.uid, group=self.group, auth_key=self.auth_key)
        self.assertFalse(subprocess.run("bash -c '[[ -f /tmp/i_was_hacked ]]'", capture_output=True, shell=True).returncode == 0)
        self.assertEqual(subprocess.run("bash -c 'grep \"root\" /etc/passwd'", capture_output=True, shell=True).returncode, 0)
        self.assertNotEqual(subprocess.run("bash -c 'grep \"new_user_test\" /etc/passwd'", capture_output=True, shell=True).returncode, 0)


    def test_check_security_of_request___shell_sanitation_2(self):
        create.create_user_check_security_add_keys_and_handle_exception(username="new_user_test && touch /tmp/i_was_hacked", uid=self.uid, group=self.group, auth_key=self.auth_key)
        self.assertFalse(subprocess.run("bash -c '[[ -f /tmp/i_was_hacked ]]'", capture_output=True, shell=True).stdout == 0)
        self.assertNotEqual(subprocess.run("bash -c 'grep \"new_user_test\" /etc/passwd'", capture_output=True, shell=True).returncode, 0)


if __name__ == '__main__':
    unittest.main()
