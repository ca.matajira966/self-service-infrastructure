# Self-Service Infrastructure
![alt text](.img/selfservice.png "Self-Service Infrastructure")

## Objective
Linux System Administrators are constantly interrupted with tasks that could be delegated to someone else. For example:

- Create a new user in X, Y and Z server
- Delete user (former employee). 
- Update user SSH keys.
- Create user in the SFTP.

This project proposes a way of delegating this kind of tasks to managers or lead developers: 
The idea is to create a web application so that users can self-serve infrastructure services.
For the web application I use Flask, to make the application look good I use Bootstrap.
To handle the connection between the client and the servers I use Celery.

Currently, the only service supported is to 'Create a User'. The script to do it is a Python wrapper for 'useradd' written with TDD.

## The Stack
```
Python 3.5
Flask
RabbitMQ
MySQL
```
## Running
```
docker-compose up
```
Then, the Flask Development Service is exposed at localhost:8080

## Author
* Camilo MATAJIRA see www.camilomatajira.com
