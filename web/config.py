import os

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'
    SQLALCHEMY_DATABASE_URI = 'mysql://celery_user:celery_pass@mysql:3306/celery'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
